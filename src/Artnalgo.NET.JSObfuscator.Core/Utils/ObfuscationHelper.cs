﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: ObfuscationHelper.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
namespace Artnalgo.NET.JSObfuscator.Core
{
    public static class ObfuscationHelper
    {
        /// <summary>
        /// Prepares the JavaScript obfuscation command by inserting the original code into the template.
        /// </summary>
        /// <returns>The prepared obfuscation command with the original code inserted.</returns>
        public static IObfuscatorOptions MapLevelToOptions(ObfuscationLevel level)
        {
            IObfuscatorOptions options = new DefaultPresetHighPerformanceOptions();
            switch (level)
            {
                case ObfuscationLevel.DefaultPresetHighPerformanceOptions:
                    break;
                case ObfuscationLevel.HighObfuscationLowPerformanceOptions:
                    options = new HighObfuscationLowPerformanceOptions();
                    break;
                case ObfuscationLevel.LowObfuscationHighPerformanceOptions:
                    options = new LowObfuscationHighPerformanceOptions();
                    break;
                case ObfuscationLevel.MediumObfuscationOptimalPerformanceOptions:
                    options = new MediumObfuscationOptimalPerformanceOptions();
                    break;
            }

            return options; 
        }
    }
}

