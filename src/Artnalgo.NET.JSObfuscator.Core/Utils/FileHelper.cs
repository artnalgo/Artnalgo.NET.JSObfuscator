﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: FileHelper.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
namespace Artnalgo.NET.JSObfuscator.Core
{
	internal class FileHelper
	{
        /// <summary>
        /// Reads content from a file located at a given relative file path.
        /// </summary>
        /// <param name="relativeFilePath">The relative path to the file from the test execution directory.</param>
        /// <returns>The content of the file as a string.</returns>
        /// <exception cref="FileNotFoundException">Thrown if the file does not exist at the specified path.</exception>
        public static async Task<string> ReadFromFile(string relativeFilePath)
        {
            if (string.IsNullOrWhiteSpace(relativeFilePath))
            {
                throw new ArgumentException("File path cannot be null or whitespace.", nameof(relativeFilePath));
            }

            string basePath = AppDomain.CurrentDomain.BaseDirectory;
            string fullPath = Path.Combine(basePath, relativeFilePath);

            if (!File.Exists(fullPath))
            {
                throw new FileNotFoundException($"The file {fullPath} was not found.");
            }

            return await File.ReadAllTextAsync(fullPath);
        }


        public static async Task WriteToFile(string relativeFilePath, string content)
        {
            if (string.IsNullOrWhiteSpace(relativeFilePath))
            {
                throw new ArgumentException("File path cannot be null or whitespace.", nameof(relativeFilePath));
            }

            string basePath = AppDomain.CurrentDomain.BaseDirectory;
            string fullPath = Path.Combine(basePath, relativeFilePath);

            await File.WriteAllTextAsync(fullPath, content);
        }
    }
}

