﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: ObfuscatorConsts.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
namespace Artnalgo.NET.JSObfuscator.Core
{
	internal static class ObfuscatorConsts
	{
        public const string OBFUSCATION_CODE_VAR = "originalCode";
        public const string OBFUSCATION_OPTIONS_VAR = "@_OPTIONS_";
        public const string OBFUSCATION_JAVASCRIPT_EXECUTION_SCRIPT = "obfuscationResult.getObfuscatedCode()";
        public const string OBFUSCATION_JAVASCRIPT_NAMESPACE = "Artnalgo.NET.JSObfuscator.Core.JavascriptLib.index.js";
        private const string CMD_TEMPLATE = $"var obfuscationResult = JavaScriptObfuscator.obfuscate({OBFUSCATION_CODE_VAR}, {OBFUSCATION_OPTIONS_VAR});";
        public static readonly string OBFUSCATION_JAVASCRIPT_CMD_TEMPLATE = $@"{Environment.NewLine}{CMD_TEMPLATE}";
    }
}

