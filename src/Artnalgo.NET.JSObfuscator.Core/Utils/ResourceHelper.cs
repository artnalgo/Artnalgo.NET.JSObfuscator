﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: ResourceHelper.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
using System.Reflection;

namespace Artnalgo.NET.JSObfuscator.Core
{
    public class ResourceHelper
    {
        public static string ReadResource(string resourceName)
        {

            Assembly assembly = Assembly.GetExecutingAssembly();

            string? resourcePath = assembly.GetManifestResourceNames()
                .SingleOrDefault(str => str.EndsWith(resourceName, StringComparison.Ordinal));

            if (resourcePath == null)
            {
                throw new FileNotFoundException($"Resource not found.");
            }

            using Stream? stream = assembly.GetManifestResourceStream(resourcePath);
            if (stream == null)
            {
                throw new InvalidOperationException($"Could not load the resource.");
            }

            using StreamReader reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }
    }
}

