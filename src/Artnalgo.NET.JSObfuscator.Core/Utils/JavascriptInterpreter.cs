﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: JavascriptInterpreter.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using Esprima.Ast;
using Jint;


namespace Artnalgo.NET.JSObfuscator.Core
{
    /// <summary>
    /// Provides functionality to execute JavaScript code asynchronously using the Jint JavaScript engine.
    /// </summary>
    internal class JavascriptInterpreter
    {


        Engine engine;


        public JavascriptInterpreter()
        {
            engine = new Engine(cfg => cfg.AllowClr());
        }



        public void setEngineValue<T>(string key, T value)
        {
            this.engine.SetValue(key, value);
        }


        /// <summary>
        /// Executes a string of JavaScript code with an optional execution part, asynchronously.
        /// </summary>
        /// <param name="javascriptCode">The base JavaScript code to set up the environment or define functions.</param>
        /// <param name="executionScript">The specific script or function call to execute within the base script context.</param>
        /// <param name="appendScript">The additional script to append directly after the base script content but before the execution script. Default is empty.</param>
        /// <returns>A Task that represents the asynchronous operation, containing the result of the JavaScript execution as a string.</returns>
        /// <summary>
        /// Executes a string of JavaScript code with an optional execution part, asynchronously.
        /// </summary>
        /// <param name="javascriptCode">The base JavaScript code to set up the environment or define functions.</param>
        /// <param name="executionScript">The specific script or function call to execute within the base script context.</param>
        /// <param name="appendScript">The additional script to append directly after the base script content but before the execution script. Default is empty.</param>
        /// <returns>A Task that represents the asynchronous operation, containing the result of the JavaScript execution as a string.</returns>
        public async Task<string> ExecuteJavaScript(string javascriptCode, string executionScript = "", string appendScript = "")
        {
            if (string.IsNullOrWhiteSpace(javascriptCode))
            {
                throw new ArgumentException("JavaScript code cannot be null or whitespace.", nameof(javascriptCode));
            }
            if (!string.IsNullOrWhiteSpace(executionScript))
            {
                executionScript = $"return {executionScript};";
            }

            string fullScript = $"{javascriptCode} {appendScript} {executionScript}".Trim();

            return await EvaluateJavascript(fullScript);
        }

        /// <summary>
        /// Reads JavaScript code from a file, optionally appends additional script, then appends an execution script, and executes it asynchronously.
        /// </summary>
        /// <param name="relativeFilePath">The relative path to the file containing the JavaScript code.</param>
        /// <param name="executionScript">The specific script or function call to format as a return statement and execute after appending the appendScript. Default is empty.</param>
        /// <param name="appendScript">The additional script to append directly after the file's content but before the execution script. Default is empty.</param>
        /// <returns>A Task that represents the asynchronous operation, containing the result of the combined JavaScript execution as a string.</returns>
        /// <exception cref="ArgumentException">Thrown when the file path is null or whitespace.</exception>
        /// <exception cref="FileNotFoundException">Thrown when the specified file cannot be found.</exception>
        public async Task<string> ExecuteJavaScriptFile(string relativeFilePath, string executionScript = "", string appendScript = "")
        {

            string scriptFromFile = await FileHelper.ReadFromFile(relativeFilePath);

            if (!string.IsNullOrWhiteSpace(executionScript))
            {
                executionScript = $"return {executionScript};";
            }

            string fullScript = $"{scriptFromFile} {appendScript} {executionScript}".Trim();

            return await EvaluateJavascript(fullScript);
        }


        /// <summary>
        /// Evaluates the provided JavaScript code using the Jint engine.
        /// </summary>
        /// <param name="script">The JavaScript code to evaluate.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation, containing the result of the JavaScript evaluation as a string.</returns>
        /// <exception cref="JavaScriptEvaluationException">Thrown when an error occurs during the evaluation of the JavaScript code.</exception>
        private async Task<string> EvaluateJavascript(string script)
        {
            return await Task.Run(() =>
            {
                try
                {
                    engine.SetValue("self", engine.Global);
                    var result = engine.Evaluate(script)?.ToString();
                    return result ?? throw new JavaScriptEvaluationException("Script evaluated to null.");
                }
                catch (Jint.Runtime.JavaScriptException ex)
                {
                    throw new JavaScriptEvaluationException($"Script execution failed: {ex.Message}", ex);
                }
                catch (Exception ex)
                {
                    throw new JavaScriptEvaluationException($"Execution failed: {ex.Message}", ex);
                }
            });
        }
    }
}