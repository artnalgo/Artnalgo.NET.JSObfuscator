﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: JavaScriptObfuscatorOptions.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------

namespace Artnalgo.NET.JSObfuscator.Core
{
    /// <summary>
    /// Represents the configuration options for JavaScript obfuscation, offering various ways to transform and protect the code.
    /// </summary>
    public class JavaScriptObfuscatorOptions : IObfuscatorOptions
    {
        /// <summary>
        /// Minifies the code by removing whitespace and shortening variable names. Default is true.
        /// </summary>
        public bool Compact { get; set; } = true;

        /// <summary>
        /// Flattens the code flow, making it harder to understand the logic. Affects performance. Default is false.
        /// </summary>
        public bool ControlFlowFlattening { get; set; } = false;

        /// <summary>
        /// The probability that control flow flattening will be applied to any given node. Default is 0.75.
        /// </summary>
        public double ControlFlowFlatteningThreshold { get; set; } = 0.75;

        /// <summary>
        /// Injects random blocks of dead code to confuse deobfuscation tools. Affects performance. Default is false.
        /// </summary>
        public bool DeadCodeInjection { get; set; } = false;

        /// <summary>
        /// The probability that dead code injection will be applied to any given node. Default is 0.4.
        /// </summary>
        public double DeadCodeInjectionThreshold { get; set; } = 0.4;

        /// <summary>
        /// Enables protection mechanisms that make it harder to use the browser's debugger. Default is false.
        /// </summary>
        public bool DebugProtection { get; set; } = false;

        /// <summary>
        /// Causes the debugger to freeze if debug protection is triggered, specified in milliseconds. Default is 0.
        /// </summary>
        public int DebugProtectionInterval { get; set; } = 0;

        /// <summary>
        /// Disables the use of 'console.log', 'console.info', 'console.error', and 'console.warn' by replacing them with empty functions. Default is true.
        /// </summary>
        public bool DisableConsoleOutput { get; set; } = true;

        /// <summary>
        /// Strategy used for renaming variables. Can be 'hexadecimal' or 'mangled'. Default is 'hexadecimal'.
        /// </summary>
        public string IdentifierNamesGenerator { get; set; } = IdentifierNamesGeneratorType.HEXADECIMAL.GetDescription();

        /// <summary>
        /// Enables logging to the console during obfuscation. Useful for debugging. Default is false.
        /// </summary>
        public bool Log { get; set; } = false;

        /// <summary>
        /// Converts literal numbers into expressions to make them harder to read. Default is false.
        /// </summary>
        public bool NumbersToExpressions { get; set; } = false;

        /// <summary>
        /// Renames global objects and functions to make it harder to access them from external scripts. Default is false.
        /// </summary>
        public bool RenameGlobals { get; set; } = false;

        /// <summary>
        /// Makes the obfuscated code resilient to formatting and variable renaming. Default is false.
        /// </summary>
        internal bool SelfDefending { get; set; } = false;

        /// <summary>
        /// Simplifies the code after obfuscation to improve performance and readability. Default is true.
        /// </summary>
        public bool Simplify { get; set; } = true;

        /// <summary>
        /// Splits strings into smaller chunks and encodes them to make static analysis harder. Default is false.
        /// </summary>
        public bool SplitStrings { get; set; } = false;

        /// <summary>
        /// The minimum length of a string for it to be split into chunks. Default is 10.
        /// </summary>
        public int SplitStringsChunkLength { get; set; } = 10;

        /// <summary>
        /// Stores strings in a special array and replaces literal strings in the code with references to this array. Default is true.
        /// </summary>
        public bool StringArray { get; set; } = true;

        /// <summary>
        /// Encodes the string array making it harder to extract strings. Can be empty, 'base64', 'rc4'. Default is empty.
        /// </summary>
        public string[] StringArrayEncoding { get; set; } = new string[] { };

        /// <summary>
        /// Transforms calls to the string array by splitting them, making the code harder to understand. Default is false.
        /// </summary>
        public bool StringArrayCallsTransform { get; set; } = false;

        /// <summary>
        /// The probability that calls to the string array will be transformed. Default is 0.5.
        /// </summary>
        public double StringArrayCallsTransformThreshold { get; set; } = 0.5;

        /// <summary>
        /// Shifts the string array indexes making it harder to reference the original strings. Default is true.
        /// </summary>
        public bool StringArrayIndexShift { get; set; } = true;

        /// <summary>
        /// Randomly rotates the string array, making it harder to predict the order of the strings. Default is true.
        /// </summary>
        public bool StringArrayRotate { get; set; } = true;

        /// <summary>
        /// Shuffles the string array, further obfuscating the string placement. Default is true.
        /// </summary>
        public bool StringArrayShuffle { get; set; } = true;

        /// <summary>
        /// The number of times the string array is wrapped into functions to make indexing more complex. Default is 1.
        /// </summary>
        public int StringArrayWrappersCount { get; set; } = 1;

        /// <summary>
        /// Allows the string array wrappers to call each other, creating a chain of calls. Default is true.
        /// </summary>
        public bool StringArrayWrappersChainedCalls { get; set; } = true;

        /// <summary>
        /// The maximum number of parameters for the string array wrappers. Default is 2.
        /// </summary>
        public int StringArrayWrappersParametersMaxCount { get; set; } = 2;

        /// <summary>
        /// The type of wrappers used for the string array ('variable', 'function'). Default is 'variable'.
        /// </summary>
        public string StringArrayWrappersType { get; set; } = ArrayWrappersType.VARIABLE.GetDescription();

        /// <summary>
        /// The threshold for including strings in the string array. Between 0 and 1. Default is 0.75.
        /// </summary>
        public double StringArrayThreshold { get; set; } = 0.75;




        /// <summary>
        /// Type: boolean Default: false Enables transformation of object keys. Default is false.
        /// </summary>
        public bool TransformObjectKeys { get; set; } = false;

        /// <summary>
        /// Encodes strings and regex patterns using Unicode escape sequences. Default is false.
        /// </summary>
        public bool UnicodeEscapeSequence { get; set; } = false;

        string IObfuscatorOptions.Serialize()
        {
            return JavaScriptObfuscatorOptionsSerializer.Serialize(this);
        }
    }
}
