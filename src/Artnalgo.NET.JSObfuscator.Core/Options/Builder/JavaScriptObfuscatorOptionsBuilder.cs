﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Artnalgo.NET.JSObfuscator.Core
{
    /// <summary>
    /// Builder class for creating instances of <see cref="JavaScriptObfuscatorOptions"/>.
    /// This class follows the builder pattern, allowing for a fluent configuration of obfuscation options.
    /// </summary>
    public class JavaScriptObfuscatorOptionsBuilder : IJavaScriptObfuscatorOptionsBuilder
    {
        private readonly JavaScriptObfuscatorOptions _options = new JavaScriptObfuscatorOptions();

        public IJavaScriptObfuscatorOptionsBuilder SetCompact(bool value)
        {
            _options.Compact = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetControlFlowFlattening(bool value, double threshold = 0.75)
        {
            _options.ControlFlowFlattening = value;
            _options.ControlFlowFlatteningThreshold = threshold;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetDeadCodeInjection(bool value, double threshold = 0.4)
        {
            _options.DeadCodeInjection = value;
            _options.DeadCodeInjectionThreshold = threshold;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetDebugProtection(bool value, int interval = 0)
        {
            _options.DebugProtection = value;
            _options.DebugProtectionInterval = interval;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetDisableConsoleOutput(bool value)
        {
            _options.DisableConsoleOutput = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetIdentifierNamesGenerator(string value)
        {
            _options.IdentifierNamesGenerator = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetLog(bool value)
        {
            _options.Log = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetNumbersToExpressions(bool value)
        {
            _options.NumbersToExpressions = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetRenameGlobals(bool value)
        {
            _options.RenameGlobals = value;
            return this;
        }

    
        public IJavaScriptObfuscatorOptionsBuilder SetSimplify(bool value)
        {
            _options.Simplify = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetSplitStrings(bool value, int chunkLength = 10)
        {
            _options.SplitStrings = value;
            _options.SplitStringsChunkLength = chunkLength;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetStringArray(bool value, bool rotate = true, bool shuffle = true, double threshold = 0.75)
        {
            _options.StringArray = value;
            _options.StringArrayRotate = rotate;
            _options.StringArrayShuffle = shuffle;
            _options.StringArrayThreshold = threshold;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetStringArrayCallsTransform(bool value, double threshold = 0.5)
        {
            _options.StringArrayCallsTransform = value;
            _options.StringArrayCallsTransformThreshold = threshold;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetStringArrayEncoding(string[] value)
        {
            _options.StringArrayEncoding = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetStringArrayIndexShift(bool value)
        {
            _options.StringArrayIndexShift = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetStringArrayRotate(bool value)
        {
            _options.StringArrayRotate = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetStringArrayShuffle(bool value)
        {
            _options.StringArrayShuffle = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetStringArrayWrappersCount(int value)
        {
            _options.StringArrayWrappersCount = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetStringArrayWrappersChainedCalls(bool value)
        {
            _options.StringArrayWrappersChainedCalls = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetStringArrayWrappersParametersMaxCount(int value)
        {
            _options.StringArrayWrappersParametersMaxCount = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetStringArrayWrappersType(ArrayWrappersType type)
        {
            _options.StringArrayWrappersType = type.GetDescription();
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetStringArrayThreshold(double value)
        {
            _options.StringArrayThreshold = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetUnicodeEscapeSequence(bool value)
        {
            _options.UnicodeEscapeSequence = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetControlFlowFlatteningThreshold(double value)
        {
            _options.ControlFlowFlatteningThreshold = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetDeadCodeInjectionThreshold(double value)
        {
            _options.DeadCodeInjectionThreshold = value;
            return this;
        }

        public IJavaScriptObfuscatorOptionsBuilder SetSplitStringsChunkLength(int value)
        {
            _options.SplitStringsChunkLength = value;
            return this;
        }


        public IJavaScriptObfuscatorOptionsBuilder SetTransformObjectKeys(bool value)
        {
            _options.TransformObjectKeys = value;
            return this;
        }
        
        /// <summary>
        /// Completes the build process and returns a fully configured <see cref="JavaScriptObfuscatorOptions"/> instance.
        /// </summary>
        /// <returns>A fully configured instance of <see cref="JavaScriptObfuscatorOptions"/>.</returns>
        public JavaScriptObfuscatorOptions Build()
        {
            return _options;
        }
    }
}
