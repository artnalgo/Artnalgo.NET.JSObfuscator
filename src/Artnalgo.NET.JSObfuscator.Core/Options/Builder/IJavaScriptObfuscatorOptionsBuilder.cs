﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: IJavaScriptObfuscatorOptionsBuilder.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
namespace Artnalgo.NET.JSObfuscator.Core
{
    /// <summary>
    /// Defines a builder interface for constructing <see cref="JavaScriptObfuscatorOptions"/> instances.
    /// This interface provides methods for setting various obfuscation options, allowing for a customizable obfuscation configuration.
    /// </summary>
    public interface IJavaScriptObfuscatorOptionsBuilder
    {
        IJavaScriptObfuscatorOptionsBuilder SetCompact(bool value);
        IJavaScriptObfuscatorOptionsBuilder SetControlFlowFlattening(bool value, double threshold = 0.75);
        IJavaScriptObfuscatorOptionsBuilder SetDeadCodeInjection(bool value, double threshold = 0.4);
        IJavaScriptObfuscatorOptionsBuilder SetDebugProtection(bool value, int interval = 0);
        IJavaScriptObfuscatorOptionsBuilder SetDisableConsoleOutput(bool value);
        IJavaScriptObfuscatorOptionsBuilder SetIdentifierNamesGenerator(string value);
        IJavaScriptObfuscatorOptionsBuilder SetLog(bool value);
        IJavaScriptObfuscatorOptionsBuilder SetNumbersToExpressions(bool value);
        IJavaScriptObfuscatorOptionsBuilder SetRenameGlobals(bool value);
        IJavaScriptObfuscatorOptionsBuilder SetSimplify(bool value);
        IJavaScriptObfuscatorOptionsBuilder SetSplitStrings(bool value, int chunkLength = 10);
        IJavaScriptObfuscatorOptionsBuilder SetStringArray(bool value, bool rotate = true, bool shuffle = true, double threshold = 0.75);
        IJavaScriptObfuscatorOptionsBuilder SetStringArrayCallsTransform(bool value, double threshold = 0.5);
        IJavaScriptObfuscatorOptionsBuilder SetStringArrayEncoding(string[] value);
        IJavaScriptObfuscatorOptionsBuilder SetStringArrayIndexShift(bool value);
        IJavaScriptObfuscatorOptionsBuilder SetStringArrayRotate(bool value);
        IJavaScriptObfuscatorOptionsBuilder SetStringArrayShuffle(bool value);
        IJavaScriptObfuscatorOptionsBuilder SetStringArrayWrappersCount(int value);
        IJavaScriptObfuscatorOptionsBuilder SetStringArrayWrappersChainedCalls(bool value);
        IJavaScriptObfuscatorOptionsBuilder SetStringArrayWrappersParametersMaxCount(int value);
        IJavaScriptObfuscatorOptionsBuilder SetStringArrayWrappersType(ArrayWrappersType type);
        IJavaScriptObfuscatorOptionsBuilder SetStringArrayThreshold(double value);
        IJavaScriptObfuscatorOptionsBuilder SetUnicodeEscapeSequence(bool value);
        IJavaScriptObfuscatorOptionsBuilder SetControlFlowFlatteningThreshold(double value);
        IJavaScriptObfuscatorOptionsBuilder SetDeadCodeInjectionThreshold(double value);
        IJavaScriptObfuscatorOptionsBuilder SetSplitStringsChunkLength(int value);
        IJavaScriptObfuscatorOptionsBuilder SetTransformObjectKeys(bool value);
        JavaScriptObfuscatorOptions Build();
    }
}

