﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: JavaScriptObfuscatorOptionsSerializer.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
using System.Text.Json;

namespace Artnalgo.NET.JSObfuscator.Core
{
    public class JavaScriptObfuscatorOptionsSerializer
    {
        public static string Serialize(JavaScriptObfuscatorOptions options)
        {
            var jsonObject = new Dictionary<string, object>
            {
                {"compact", options.Compact},
                {"controlFlowFlattening", options.ControlFlowFlattening},
                {"controlFlowFlatteningThreshold", options.ControlFlowFlatteningThreshold},
                {"deadCodeInjection", options.DeadCodeInjection},
                {"deadCodeInjectionThreshold", options.DeadCodeInjectionThreshold},
                {"debugProtection", options.DebugProtection},
                {"debugProtectionInterval", options.DebugProtectionInterval},
                {"disableConsoleOutput", options.DisableConsoleOutput},
                {"identifierNamesGenerator", options.IdentifierNamesGenerator},
                {"log", options.Log},
                {"numbersToExpressions", options.NumbersToExpressions},
                {"renameGlobals", options.RenameGlobals},
                {"selfDefending", options.SelfDefending},
                {"simplify", options.Simplify},
                {"splitStrings", options.SplitStrings},
                {"splitStringsChunkLength", options.SplitStringsChunkLength},
                {"stringArray", options.StringArray},
                {"stringArrayCallsTransform", options.StringArrayCallsTransform},
                {"stringArrayCallsTransformThreshold", options.StringArrayCallsTransformThreshold},
                {"stringArrayEncoding", options.StringArrayEncoding},
                {"stringArrayIndexShift", options.StringArrayIndexShift},
                {"stringArrayRotate", options.StringArrayRotate},
                {"stringArrayShuffle", options.StringArrayShuffle},
                {"stringArrayWrappersCount", options.StringArrayWrappersCount},
                {"stringArrayWrappersChainedCalls", options.StringArrayWrappersChainedCalls},
                {"stringArrayWrappersParametersMaxCount", options.StringArrayWrappersParametersMaxCount},
                {"stringArrayWrappersType", options.StringArrayWrappersType},
                {"stringArrayThreshold", options.StringArrayThreshold},
                {"transformObjectKeys", options.TransformObjectKeys},
                {"unicodeEscapeSequence", options.UnicodeEscapeSequence}
            };

            return JsonSerializer.Serialize(jsonObject, new JsonSerializerOptions { WriteIndented = true });
        }
    }
}

