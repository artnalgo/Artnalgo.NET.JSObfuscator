﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: ObfuscatorConfigFactory.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
namespace Artnalgo.NET.JSObfuscator.Core
{
    public static class ObfuscatorConfigFactory
    {
        public static IObfuscatorOptions CreatePreset(ObfuscationLevel level)
        {
            switch (level)
            {
                case ObfuscationLevel.DefaultPresetHighPerformanceOptions:
                    return new DefaultPresetHighPerformanceOptions();

                case ObfuscationLevel.HighObfuscationLowPerformanceOptions:
                    return new HighObfuscationLowPerformanceOptions();

                case ObfuscationLevel.LowObfuscationHighPerformanceOptions:
                    return new LowObfuscationHighPerformanceOptions();

                case ObfuscationLevel.MediumObfuscationOptimalPerformanceOptions:
                    return new MediumObfuscationOptimalPerformanceOptions();
                default:
                    throw new ArgumentException("Invalid obfuscation level", nameof(level));
            }
        }
    }
}

