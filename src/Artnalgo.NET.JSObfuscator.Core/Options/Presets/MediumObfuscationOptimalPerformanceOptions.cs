﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: MediumObfuscationOptimalPerformanceOptions.cs
// Author: ozmpai
//
// Licensed under the Apache License; Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http=//www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing; software
// distributed under the License is distributed on an "AS IS" BASIS;
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND; either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//

namespace Artnalgo.NET.JSObfuscator.Core
{
    public class MediumObfuscationOptimalPerformanceOptions: JavaScriptObfuscatorOptions
    {
        public MediumObfuscationOptimalPerformanceOptions()
        {
            Compact = true;
            ControlFlowFlattening = true;
            ControlFlowFlatteningThreshold = 0.75;
            DeadCodeInjection = true;
            DeadCodeInjectionThreshold = 0.4;
            DebugProtection = false;
            DebugProtectionInterval = 0;
            DisableConsoleOutput = false;
            IdentifierNamesGenerator = IdentifierNamesGeneratorType.HEXADECIMAL.GetDescription();
            Log = false;
            NumbersToExpressions = true;
            RenameGlobals = false;
            SelfDefending = false;
            Simplify = true;
            SplitStrings = true;
            SplitStringsChunkLength = 10;
            StringArray = true;
            StringArrayCallsTransform = true;
            StringArrayCallsTransformThreshold = 0.75;
            StringArrayEncoding = new string[] { StringArrayEncodingType.BASE64.GetDescription() };
            StringArrayIndexShift = true;
            StringArrayRotate = true;
            StringArrayShuffle = true;
            StringArrayWrappersCount = 2;
            StringArrayWrappersChainedCalls = true;
            StringArrayWrappersParametersMaxCount = 4;
            StringArrayWrappersType = ArrayWrappersType.FUNCTION.GetDescription();
            StringArrayThreshold = 0.75;
            TransformObjectKeys = true;
            UnicodeEscapeSequence = false;
        }
  
    }

}



