﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: HighObfuscationLowPerformanceOptions.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
namespace Artnalgo.NET.JSObfuscator.Core
{
    /// <summary>
    /// High obfuscation, low performance
    /// </summary>
	public class HighObfuscationLowPerformanceOptions : JavaScriptObfuscatorOptions
    {
        public HighObfuscationLowPerformanceOptions()
        {
            Compact = true;
            ControlFlowFlattening = true;
            ControlFlowFlatteningThreshold = 1;
            DeadCodeInjection = true;
            DeadCodeInjectionThreshold = 1;
            DebugProtection = true;
            DebugProtectionInterval = 4000;
            DisableConsoleOutput = false;
            IdentifierNamesGenerator = IdentifierNamesGeneratorType.HEXADECIMAL.GetDescription();
            Log = false;
            NumbersToExpressions = true;
            RenameGlobals = false;
            SelfDefending = false;
            Simplify = true;
            SplitStrings = true;
            SplitStringsChunkLength = 5;
            StringArray = true;
            StringArrayCallsTransform = true;
            StringArrayEncoding = new string[] { StringArrayEncodingType.RC4.GetDescription() };
            StringArrayIndexShift = true;
            StringArrayRotate = true;
            StringArrayShuffle = true;
            StringArrayWrappersCount = 5;
            StringArrayWrappersChainedCalls = true;
            StringArrayWrappersParametersMaxCount = 5;
            StringArrayWrappersType = ArrayWrappersType.FUNCTION.GetDescription();
            StringArrayThreshold = 1;
            TransformObjectKeys = true;
            UnicodeEscapeSequence = false;




        }
    }
}

