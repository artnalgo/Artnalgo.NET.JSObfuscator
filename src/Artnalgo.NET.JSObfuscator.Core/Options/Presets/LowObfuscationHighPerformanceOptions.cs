﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: LowObfuscationHighPerformanceOptions.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
namespace Artnalgo.NET.JSObfuscator.Core
{
    public class LowObfuscationHighPerformanceOptions : JavaScriptObfuscatorOptions
    {
        public LowObfuscationHighPerformanceOptions()
        {
            Compact = true;
            ControlFlowFlattening = false;
            DeadCodeInjection = false;
            DebugProtection = false;
            DebugProtectionInterval = 0;
            DisableConsoleOutput = false;
            IdentifierNamesGenerator = IdentifierNamesGeneratorType.HEXADECIMAL.GetDescription();
            Log = false;
            NumbersToExpressions = false;
            RenameGlobals = false;
            SelfDefending = false;
            Simplify = true;
            SplitStrings = false;
            StringArray = true;
            StringArrayCallsTransform = false;
            StringArrayEncoding = new string[] { };
            StringArrayIndexShift = true;
            StringArrayRotate = true;
            StringArrayShuffle = true;
            StringArrayWrappersCount = 1;
            StringArrayWrappersChainedCalls = true;
            StringArrayWrappersParametersMaxCount = 2;
            StringArrayWrappersType = ArrayWrappersType.VARIABLE.GetDescription();
            StringArrayThreshold = 0.75;
            UnicodeEscapeSequence = false;

        }
    }
}

