﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: IObfuscatorOptions.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//

namespace Artnalgo.NET.JSObfuscator.Core
{
	public interface IObfuscatorOptions
	{
        bool Compact { get; set; }
        bool ControlFlowFlattening { get; set; }
        double ControlFlowFlatteningThreshold { get; set; }
        bool DeadCodeInjection { get; set; }
        double DeadCodeInjectionThreshold { get; set; }
        bool DebugProtection { get; set; }
        int DebugProtectionInterval { get; set; }
        bool DisableConsoleOutput { get; set; }
        string IdentifierNamesGenerator { get; set; }
        bool Log { get; set; }
        bool NumbersToExpressions { get; set; }
        bool RenameGlobals { get; set; }
        bool Simplify { get; set; }
        bool SplitStrings { get; set; }
        int SplitStringsChunkLength { get; set; }
        bool StringArray { get; set; }
        string[] StringArrayEncoding { get; set; }
        bool StringArrayCallsTransform { get; set; }
        double StringArrayCallsTransformThreshold { get; set; }
        bool StringArrayIndexShift { get; set; }
        bool StringArrayRotate { get; set; }
        bool StringArrayShuffle { get; set; }
        int StringArrayWrappersCount { get; set; }
        bool StringArrayWrappersChainedCalls { get; set; }
        int StringArrayWrappersParametersMaxCount { get; set; }
        string StringArrayWrappersType { get; set; }
        double StringArrayThreshold { get; set; }
        bool TransformObjectKeys { get; set; }
        bool UnicodeEscapeSequence { get; set; }

        /// <summary>
        /// Serializes the options object to a JSON string.
        /// </summary>
        /// <returns>JSON string representation of the options object.</returns>
        string Serialize();
    }
}

