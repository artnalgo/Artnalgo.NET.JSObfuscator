﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: ObfuscationLevel.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
namespace Artnalgo.NET.JSObfuscator.Core
{
	public enum ObfuscationLevel
	{
        //Default preset, High performance. The performance will be much slower than without obfuscation
        DefaultPresetHighPerformanceOptions,


        //High obfuscation, low performance. The performance will be slower than without obfuscation
        HighObfuscationLowPerformanceOptions,

        //Low obfuscation, High performance. The performance will be at a relatively normal level
        LowObfuscationHighPerformanceOptions,

        //Medium obfuscation, optimal performance. Default preset, High performance
        MediumObfuscationOptimalPerformanceOptions
    }



}

