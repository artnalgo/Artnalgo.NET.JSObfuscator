﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: Obfuscator.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
using System.Threading.Tasks;
using System.IO;
using Jint;

namespace Artnalgo.NET.JSObfuscator.Core
{
    /// <summary>
    /// Provides functionality to obfuscate JavaScript code.
    /// </summary>
    public class JavascriptObfuscator: IJavascriptObfuscator
    {
        private readonly JavascriptInterpreter _interpreter;

        public JavascriptObfuscator()
        {
            _interpreter = new JavascriptInterpreter();
        }

        public async Task<string> ObfuscateCode(string originalCode, ObfuscationLevel level = ObfuscationLevel.DefaultPresetHighPerformanceOptions)
        {
            if (string.IsNullOrWhiteSpace(originalCode)) { throw new JavaScriptException("Original code cannot be null or whitespace."); }
            IObfuscatorOptions obfuscationCommand = ObfuscationHelper.MapLevelToOptions(level);
            return await Obfucate(originalCode, obfuscationCommand);
        }

        public async Task<string> ObfuscateCode(string originalCode, IObfuscatorOptions options)
        {
            if (string.IsNullOrWhiteSpace(originalCode)) { throw new JavaScriptException("Original code cannot be null or whitespace."); }
            return await Obfucate(originalCode, options);
        }

        private async Task<string> Obfucate(string originalCode, IObfuscatorOptions options) {
         
            var jsobtr = ResourceHelper.ReadResource(ObfuscatorConsts.OBFUSCATION_JAVASCRIPT_NAMESPACE);
            if (string.IsNullOrEmpty(jsobtr)) { throw new JavaScriptLibException(); }
            _interpreter.setEngineValue(ObfuscatorConsts.OBFUSCATION_CODE_VAR, originalCode);
            string obfuscatedCode = await _interpreter.ExecuteJavaScript(jsobtr,
                ObfuscatorConsts.OBFUSCATION_JAVASCRIPT_EXECUTION_SCRIPT,
                ObfuscatorConsts.OBFUSCATION_JAVASCRIPT_CMD_TEMPLATE.Replace(ObfuscatorConsts.OBFUSCATION_OPTIONS_VAR, options.Serialize()));

            return obfuscatedCode;
        }
    }
}
