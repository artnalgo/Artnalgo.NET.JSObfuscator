﻿# .NET Javascript Obfuscator

Artnalgo.NET.JSObfuscator.Core provides advanced JavaScript obfuscation tools, enhancing .NET security by utilizing [javascript-obfuscator](https://github.com/javascript-obfuscator/javascript-obfuscator) alongside [Jint](https://github.com/sebastienros/jint), a JavaScript interpreter. This combination aids .NET developers in strengthening the protection of their JavaScript code against unauthorized access and tampering.

## Usage

**Implementation**

```csharp
using Artnalgo.NET.JSObfuscator.Core;

var obfuscator = new JavascriptObfuscator();
string originalCode = "function greet() { console.log('Hello, world!'); }";
string obfuscatedCode = obfuscator.ObfuscateCode(originalCode, ObfuscationLevel.LowObfuscationHighPerformanceOptions);
```

## Additional Documentation

- [GitLab Repository and Documentation](https://gitlab.com/artnalgo/Artnalgo.NET.JSObfuscator)

## Feedback

Your feedback is crucial to us! Please let us know your thoughts or report issues through:

- [GitLab Issues](https://gitlab.com/artnalgo/Artnalgo.NET.JSObfuscator/issues)
- Twitter: [@artnalgo](https://twitter.com/artnalgo)

## Troubleshooting

Consult our documentation for assistance with common problems. If you encounter any issues, don't hesitate to reach out through the feedback channels mentioned above.