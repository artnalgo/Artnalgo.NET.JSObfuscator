﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: JavascriptInterpreterTests.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using Artnalgo.NET.JSObfuscator.Core;

namespace Artnalgo.NET.JSObfuscator.Tests;


[TestClass]
public class JavascriptInterpreterTests
{


    /// <summary>
    /// Tests reading and executing JavaScript code from a file, verifying the expected result.
    /// This test also demonstrates handling file operations in the context of JavaScript execution.
    /// </summary>
    [TestMethod]
    public async Task MultipleOperations_ReturnsExpectedResult()
    {
        // Arrange
        var interpreter = new JavascriptInterpreter();
        string relativeFilePath = @"TestData/testScript.js";

        string testScriptFromFile = await FileHelper.ReadFromFile(relativeFilePath);

        // Expected result format: "Hello, World!;Orange;John Doe;Adult;15;50;25"
        string expectedResult = "Hello, World!;Orange;John Doe;Adult;15;50;25";

        // Act
        var result = await interpreter.ExecuteJavaScript(testScriptFromFile, "executeMultipleOperations()");

        // Assert
        Assert.AreEqual(expectedResult, result);
    }

    /// <summary>
    /// Tests executing a script with expected operations but checks against an intentionally incorrect expected result to demonstrate handling of unexpected outcomes.
    /// This method illustrates how the test behaves when the actual result does not match the modified (incorrect) expectation, serving as an example of an unhappy path in testing.
    /// </summary>
    [TestMethod]
    public async Task MultipleOperations_WithModifiedExpectation_ReturnsUnexpectedResult()
    {
        // Arrange: Read the test script from a file, assuming the script performs multiple operations.
        var interpreter = new JavascriptInterpreter();
        string relativeFilePath = @"TestData/testScript.js";
        string testScriptFromFile = await FileHelper.ReadFromFile(relativeFilePath);

        // The incorrect expected result for demonstration purposes.
        string incorrectExpectedResult = "Hello, World!;Orange;John Doe;Adult;30;50;25";

        // Act: Execute the read JavaScript code.
        var result = await interpreter.ExecuteJavaScript(testScriptFromFile, "executeMultipleOperations()");

        // Assert: Verify the result does NOT match the incorrect expected result.
        // This assertion is expected to fail, demonstrating the test's unhappy path.
        Assert.AreNotEqual(incorrectExpectedResult, result, "The result unexpectedly matched the incorrect expectation.");
    }


    /// <summary>
    /// Tests executing a simple JavaScript expression and verifies the expected result.
    /// </summary>
    [TestMethod]
    public async Task ExecuteJavaScript_ReturnsExpectedResult()
    {
        // Arrange: Initialize the interpreter and define a simple JavaScript expression.
        var interpreter = new JavascriptInterpreter();
        string testScript = "return 2 + 2;";

        // Act: Execute the JavaScript code.
        var result = await interpreter.ExecuteJavaScript(testScript);

        // Assert: Verify the execution result matches the expected value.
        Assert.AreEqual("4", result);
    }


    /// <summary>
    /// Tests reading and executing JavaScript code from a file, verifying the expected result.
    /// This test also demonstrates handling file operations in the context of JavaScript execution.
    /// </summary>
    [TestMethod]
    public async Task ExecuteJavaScriptFile_ReturnsExpectedResult()
    {
        // Arrange
    
        var interpreter = new JavascriptInterpreter();
        string relativeFilePath = @"TestData/testScript.js";
        string testScriptFromFile = await FileHelper.ReadFromFile(relativeFilePath);
        // Act
        var result = await interpreter.ExecuteJavaScript(testScriptFromFile, "SumNumbers(3, 3)");

        // Assert
        Assert.AreEqual("6", result);

    }


    /// <summary>
    /// Tests executing JavaScript code that is expected to fail, verifying the error handling mechanism.
    /// </summary>
    [TestMethod]
    public async Task ExecuteJavaScript_WithInvalidCode_ThrowsJavaScriptEvaluationException()
    {
        // Arrange
        var interpreter = new JavascriptInterpreter();
        string invalidScript = "return x + 2;"; // Assuming 'x' is undefined

        // Act and Assert
        await Assert.ThrowsExceptionAsync<JavaScriptEvaluationException>(async () =>
        {
            await interpreter.ExecuteJavaScript(invalidScript);
        }, "Expected JavaScriptEvaluationException to be thrown due to undefined variable.");
    }

    

    /// <summary>
    /// Tests the behavior when attempting to execute JavaScript from a non-existent file, expecting an exception.
    /// </summary>
    [TestMethod]
    [ExpectedException(typeof(FileNotFoundException))]
    public async Task ExecuteJavaScriptFile_WithNonExistentFile_ThrowsFileNotFoundException()
    {
        // Arrange
        var interpreter = new JavascriptInterpreter();
        string nonExistentFilePath = @"TestData\nonExistentScript.js";

        // Act
        await interpreter.ExecuteJavaScriptFile(nonExistentFilePath);

        // Since an exception is expected, there is no need for an Assert here.
    }

    /// <summary>
    /// Tests that the appendScript parameter correctly appends and executes JavaScript code 
    /// along with the base script.
    /// </summary>
    [TestMethod]
    public async Task ExecuteJavaScript_WithAppendScript_ModifiesExecution()
    {
        // Arrange
        var interpreter = new JavascriptInterpreter();
        // Base script defines a variable
        string baseScript = "let message = 'Hello';";

        // Append script modifies the variable
        string appendScript = "message += ', World';";

        // Execution script returns the modified variable
        string executionScript = "message";

        // Expected result reflects the modification made by the appendScript
        string expectedResult = "Hello, World";

        // Act
        var result = await interpreter.ExecuteJavaScript(baseScript, executionScript, appendScript);

        // Assert
        Assert.AreEqual(expectedResult, result, "The appendScript did not modify the base script's execution as expected.");
    }




}
