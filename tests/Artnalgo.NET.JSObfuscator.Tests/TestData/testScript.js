﻿function SumNumbers(number1, number2) {
    return number1 + number2;
}
function executeMultipleOperations() {
    // String concatenation
    var combinedStrings = 'Hello, ' + 'World!';

    // Array manipulation
    var fruits = ['Apple', 'Banana'];
    fruits.push('Orange');
    var lastFruit = fruits[fruits.length - 1];

    // Object property access
    var person = {
        firstName: 'John',
        lastName: 'Doe',
        age: 30
    };
    var fullName = person.firstName + ' ' + person.lastName;

    // Conditional logic
    function checkAge(age) {
        return age >= 18 ? 'Adult' : 'Minor';
    }
    var ageStatus = checkAge(20);

    // Looping over arrays
    var numbers = [1, 2, 3, 4, 5];
    var sum = 0;
    for (var i = 0; i < numbers.length; i++) {
        sum += numbers[i];
    }

    // Using JavaScript Math object
    var maxNumber = Math.max(10, 20, 30, 40, 50);

    // Function expressions
    var square = function (number) {
        return number * number;
    };
    var squareOfFive = square(5);

    // Combine results
    return combinedStrings + ';' +
        lastFruit + ';' +
        fullName + ';' +
        ageStatus + ';' +
        sum + ';' +
        maxNumber + ';' +
        squareOfFive;
}
// Expected result format: "Hello, World!;Orange;John Doe;Adult;15;50;25"

