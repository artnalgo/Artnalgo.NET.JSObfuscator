﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: AnalyticsGenerator.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
using Artnalgo.NET.JSObfuscator.Core;
using System.Diagnostics;
using System.Text;

namespace Artnalgo.NET.JSObfuscator.Tests
{
    //[TestClass]
    public class AnalyticsGenerator
	{
        //[TestMethod]
        public async Task Analytics()
        {
            var obfuscator = new JavascriptObfuscator();
            string originalCode = await FileHelper.ReadFromFile("TestData/original.json");

            var stopwatch = new Stopwatch();

            stopwatch.Start();
            string lowObfuscatedCode = await obfuscator.ObfuscateCode(originalCode, ObfuscationLevel.LowObfuscationHighPerformanceOptions);
            stopwatch.Stop();
            var lowObfuscationTime = stopwatch.Elapsed.TotalSeconds;
            await FileHelper.WriteToFile("low.json", lowObfuscatedCode);

            stopwatch.Restart();
            string defaultObfuscatedCode = await obfuscator.ObfuscateCode(originalCode, ObfuscationLevel.DefaultPresetHighPerformanceOptions);
            stopwatch.Stop();
            var defaultObfuscationTime = stopwatch.Elapsed.TotalSeconds;
            await FileHelper.WriteToFile("default.json", defaultObfuscatedCode);

            stopwatch.Restart();
            string mediumObfuscatedCode = await obfuscator.ObfuscateCode(originalCode, ObfuscationLevel.MediumObfuscationOptimalPerformanceOptions);
            stopwatch.Stop();
            var mediumObfuscationTime = stopwatch.Elapsed.TotalSeconds;
            await FileHelper.WriteToFile("medium.json", mediumObfuscatedCode);

            stopwatch.Restart();
            string highObfuscatedCode = await obfuscator.ObfuscateCode(originalCode, ObfuscationLevel.HighObfuscationLowPerformanceOptions);
            stopwatch.Stop();
            var highObfuscationTime = stopwatch.Elapsed.TotalSeconds;
            await FileHelper.WriteToFile("high.json", highObfuscatedCode);

            // Preparing the content for Analytics.md
            var analyticsContent = new StringBuilder();
            analyticsContent.AppendLine("## Analytics");
            analyticsContent.AppendLine($"Original Code Size: {Encoding.UTF8.GetByteCount(originalCode)} bytes, Character Count: {originalCode.Length}");
            analyticsContent.AppendLine($"Low Obfuscation: Size = {Encoding.UTF8.GetByteCount(lowObfuscatedCode)} bytes, Time = {lowObfuscationTime} seconds");
            analyticsContent.AppendLine($"Default Obfuscation: Size = {Encoding.UTF8.GetByteCount(defaultObfuscatedCode)} bytes, Time = {defaultObfuscationTime} seconds");
            analyticsContent.AppendLine($"Medium Obfuscation: Size = {Encoding.UTF8.GetByteCount(mediumObfuscatedCode)} bytes, Time = {mediumObfuscationTime} seconds");
            analyticsContent.AppendLine($"High Obfuscation: Size = {Encoding.UTF8.GetByteCount(highObfuscatedCode)} bytes, Time = {highObfuscationTime} seconds");

         
            // This will truncate the output to the first 100 characters for readability in this example.
            analyticsContent.AppendLine($"Low Obfuscated Code (sample): {lowObfuscatedCode.Substring(0, Math.Min(100, lowObfuscatedCode.Length))}...");
            analyticsContent.AppendLine($"Default Obfuscated Code (sample): {defaultObfuscatedCode.Substring(0, Math.Min(100, defaultObfuscatedCode.Length))}...");
            analyticsContent.AppendLine($"Medium Obfuscated Code (sample): {mediumObfuscatedCode.Substring(0, Math.Min(100, mediumObfuscatedCode.Length))}...");
            analyticsContent.AppendLine($"High Obfuscated Code (sample): {highObfuscatedCode.Substring(0, Math.Min(100, highObfuscatedCode.Length))}...");

            // Writing to Analytics.md
            await FileHelper.WriteToFile("Analytics.md", analyticsContent.ToString());
        }

    }
}

