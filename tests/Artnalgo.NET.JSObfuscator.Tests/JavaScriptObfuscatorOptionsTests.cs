﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: JavaScriptObfuscatorOptionsTests.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System.Diagnostics;
using System.Text;
using System.Text.Json;
using Artnalgo.NET.JSObfuscator.Core;

namespace Artnalgo.NET.JSObfuscator.Tests;


[TestClass]
public class JavaScriptObfuscatorOptionsTests
{
   

    [TestMethod]
    public async Task BuilderAndSerializer_ShouldProduceExpectedConfiguration()
    {
        // Arrange
        var builder = new JavaScriptObfuscatorOptionsBuilder()
            .SetCompact(true)
            .SetControlFlowFlattening(false)
            .SetControlFlowFlatteningThreshold(0.75)
            .SetDeadCodeInjection(false)
            .SetDeadCodeInjectionThreshold(0.4)
            .SetDebugProtection(false, interval: 0)
            .SetDisableConsoleOutput(true)
            .SetIdentifierNamesGenerator("hexadecimal")
            .SetLog(false)
            .SetNumbersToExpressions(false)
            .SetRenameGlobals(false)
            .SetSimplify(true)
            .SetSplitStrings(false)
            .SetSplitStringsChunkLength(10)
            .SetStringArray(true)
            .SetStringArrayCallsTransform(false, threshold: 0.5)
            .SetStringArrayEncoding(new string[] { })
            .SetStringArrayIndexShift(true)
            .SetStringArrayRotate(true)
            .SetStringArrayShuffle(true)
            .SetStringArrayWrappersCount(1)
            .SetStringArrayWrappersChainedCalls(true)
            .SetStringArrayWrappersParametersMaxCount(2)
            .SetStringArrayWrappersType(ArrayWrappersType.VARIABLE)
            .SetStringArrayThreshold(0.75)
            .SetUnicodeEscapeSequence(false);

        var options = builder.Build();

        // Act - Serialize the options to JSON
        var json = JavaScriptObfuscatorOptionsSerializer.Serialize(options);

        // Read the expected JSON from the validation file
        var validationFileText = await FileHelper.ReadFromFile("TestData/validation.json");

        // Assert
        Assert.AreEqual(validationFileText.Trim(), json.Trim(), "The serialized JSON does not match the expected validation JSON.");
    }


    /// <summary>
    /// Tests the serialization of custom JavaScript obfuscation options set via the JavaScriptObfuscatorOptionsBuilder.
    /// This test ensures that when specific obfuscation settings are applied using the builder,
    /// the resulting serialized JSON string matches the expected configuration.
    /// </summary>
    [TestMethod]
    public async Task SerializeCustomObfuscationOptions_MatchesExpectedConfiguration()
    {
        // Arrange: Create custom obfuscation options using the builder.
        var options = new JavaScriptObfuscatorOptionsBuilder()
                            .SetCompact(false)
                            .SetControlFlowFlattening(true)
                            .SetStringArrayWrappersType(ArrayWrappersType.FUNCTION)
                            .Build();

        // Act: Serialize the custom options to JSON.
        var jsonString = JavaScriptObfuscatorOptionsSerializer.Serialize(options);

        // Assert: Verify that the serialized JSON matches the expected configuration.
        var validationFileText = await FileHelper.ReadFromFile("TestData/validationCustomOptions.json");
        Assert.AreEqual(validationFileText.Trim(), jsonString.Trim(), "The serialized JSON does not match the expected custom configuration JSON.");
    }
}


