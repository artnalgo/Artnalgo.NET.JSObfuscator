﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: JavascriptObfuscatorTests.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Artnalgo.NET.JSObfuscator.Core;
using System.Threading.Tasks;

namespace Artnalgo.NET.JSObfuscator.Tests
{
    [TestClass]
    public class JavascriptObfuscatorTests
    {

        [TestMethod]
        public async Task ExecuteJavaScriptFile_ObfuscationMaintainsFunctionalEquivalence()
        {


            // Arrange
            var obfuscator = new JavascriptObfuscator();
            var interpreter = new JavascriptInterpreter();
            string relativeFilePath = @"TestData/testScript.js"; // Ensure this script returns a value for comparison

            // Read the original script from a file
            string originalScript = await FileHelper.ReadFromFile(relativeFilePath);

            // Obfuscate the original script
            string obfuscatedScript = await obfuscator.ObfuscateCode(originalScript);

            // Act
            // Execute the original script
            string originalResult = await interpreter.ExecuteJavaScript(originalScript, "");

            // Execute the obfuscated script
            string obfuscatedResult = await interpreter.ExecuteJavaScript(obfuscatedScript, "");

            // Assert
            // Check that both original and obfuscated scripts produce the same result
            Assert.AreEqual(originalResult, obfuscatedResult, "Obfuscated code did not maintain functional equivalence with the original code.");
        }


        [TestMethod]
        public async Task ObfuscateCode_ValidInput_ReturnsObfuscatedCode()
        {
            // Arrange
            var obfuscator = new JavascriptObfuscator();
            string originalCode = "function test() { console.log('Hello World'); }";

            // Act
            string obfuscatedCode = await obfuscator.ObfuscateCode(originalCode, level: ObfuscationLevel.HighObfuscationLowPerformanceOptions);

            // Assert
            Assert.IsNotNull(obfuscatedCode);
            Assert.AreNotEqual(string.Empty, obfuscatedCode);


            Assert.IsTrue(obfuscatedCode.Length > originalCode.Length, "Obfuscated code should be longer than original.");
        }


        [TestMethod]
        public async Task ObfuscateAndExecuteCode_ValidInput_FunctionalEquivalence()
        {
            // Arrange
            var obfuscator = new JavascriptObfuscator();
            var interpreter = new JavascriptInterpreter();
            string originalCode = "function add(a, b) { return a + b; } add(2, 3);";

            // Act
            string obfuscatedCode = await obfuscator.ObfuscateCode(originalCode);

            // Execute both original and obfuscated code using the interpreter
            string originalResult = await interpreter.ExecuteJavaScript(originalCode, "");
            string obfuscatedResult = await interpreter.ExecuteJavaScript(obfuscatedCode, "");

            // Assert
            Assert.IsNotNull(obfuscatedCode);
            Assert.AreNotEqual(string.Empty, obfuscatedCode);
            Assert.IsTrue(obfuscatedCode.Length > originalCode.Length, "Obfuscated code should be longer than original.");

            // Most importantly, verify that both original and obfuscated code produce the same result
            Assert.AreEqual(originalResult, obfuscatedResult, "Obfuscated code did not maintain functional equivalence with the original code.");
        }


      
    }


}
