# .NET Javascript Obfuscator

Artnalgo.NET.JSObfuscator.Core provides advanced JavaScript obfuscation tools, enhancing .NET security by utilizing [javascript-obfuscator](https://github.com/javascript-obfuscator/javascript-obfuscator) alongside [Jint](https://github.com/sebastienros/jint), a JavaScript interpreter. This combination aids .NET developers in strengthening the protection of their JavaScript code against unauthorized access and tampering.

## Key Features

- Provides a fluent C# API
- Includes predefined configurations for easy setup


## Installation

```shell
dotnet add package Artnalgo.NET.JSObfuscator.Core
```

### Usage

```csharp
using Artnalgo.NET.JSObfuscator.Core;

var obfuscator = new JavascriptObfuscator();

string originalCode = "function greet() { console.log('Hello, world!'); }";
string obfuscatedCode = await obfuscator.ObfuscateCode(originalCode, ObfuscationLevel.LowObfuscationHighPerformanceOptions);
```

## Customizing

```csharp
var options = new JavaScriptObfuscatorOptionsBuilder()
    .SetDebugProtection(true, interval: 4000) // Adds browser debugger protections.
    .SetDisableConsoleOutput(true) // Suppresses console logging.
    .SetUnicodeEscapeSequence(true) // Implements Unicode escape sequences for strings and regex patterns.
    .Build();

string obfuscatedCode = await obfuscator.ObfuscateCode(originalCode, options: options);
```

## Obfuscation Levels

- Moderate Obfuscation: A balance between performance and security
- Maximum Obfuscation: The highest level of security, considering performance
- Minimal Obfuscation: Focused on performance while offering basic security
- Balanced Approach: An optimal mix of security and performance

## Understanding Obfuscation Impact
#### These metrics illustrate how different levels of obfuscation can significantly affect the size and processing time

##### Original Code Metrics

- **Size:** 50 bytes
- **Character Count:** 50

##### Obfuscation Results Metrics

1. **Low Obfuscation**
   - **Size:** 3,039 bytes
   - **Time:** 3.806 seconds
   - **Sample:** `(function(_0x28a40d,_0x5757a5){var _0x3ba903=_0x4e10, ...`

2. **Default Obfuscation**
   - **Size:** 1,156 bytes
   - **Time:** 1.542 seconds
   - **Sample:** `(function(_0x91421d,_0x15e5a4){var _0x199419=_0x1c99, ...`

3. **Medium Obfuscation**
   - **Size:** 20,060 bytes
   - **Time:** 4.624 seconds
   - **Sample:** `function _0x121a(_0x13b2d6,_0x423722){var _0x308738=_0x5744(); ...`

4. **High Obfuscation**
   - **Size:** 112,551 bytes
   - **Time:** 14.574 seconds
   - **Sample:** `function _0x5349(){var _0x16e6df=['it3cTqya','WR7dGCkWW7q', ...`
